#https://soundcloud.com/edumusic/tito-puente-8-timbales // Tito Puente - 8 Timbales
#https://soundcloud.com/eddie-palmieri/vamonos-pal-monte-1 // Eddie Palmieri - Vamonos Pal Monte
#https://soundcloud.com/eddie-palmieri/jibarita-y-su-son-sample// ddie Palmieri - Jibarita y su Son (sample)
#https://soundcloud.com/oscar-de-leon-original/tambo-1984 // OSCAR DE LEON. "OFICIAL"
#https://soundcloud.com/oscar-de-leon-original/mi-negra-esta-cansa-en-vivo // OSCAR DE LEON. Mi Negra Esta Cansa En Vivo Desde El Poliedro. 1980
#https://soundcloud.com/tamalitos-1/la-sitiera //Sitiera - Omara Portuondo

#https://soundcloud.com/djluis-1/la-sonora-poncena // La Sonora Poncena - Nanara Cai
#https://soundcloud.com/bescor/changuiri-sonora-ponce-a // Changuiri - SONORA PONCEÑA
#https://soundcloud.com/capitalmundialdelasalsa/como-mango-sonora-ponce-a //  Como mango - Sonora Ponceña
#https://soundcloud.com/gonzalezsantana/sets/medley-sonora-poncena // Medley Sonora Poncena
#https://soundcloud.com/afrocuba/05-ran-kan-kan-mp3 // Tito Puente - Ran Kan Kan
#https://soundcloud.com/jobo-antepaz-1/arsenio-rodriguez-su-conjunto // Arsenio Rodriguez Su Conjunto 'Hay Fuego En El 23'
#https://soundcloud.com/timbamusic/ya-me-lo-dio-arsenio-rodriguez // Ya Me Lo Dio - Arsenio Rodriguez (1948) 
#https://soundcloud.com/rickygonzalez-1/la-hipocresia-y-la-falsedad // La hipocresia y la falsedad - Ray Barreto
#https://soundcloud.com/djser-j/ray-barreto-swing-la-moderna // Ray Barreto - Swing La Moderna
#https://soundcloud.com/ritmoson/fuego-y-palante-ray-barreto // Fuego y palante - ray barreto
#https://soundcloud.com/tumi-music/cienfuegos-que-bueno-baila // Benny More - Cienfuegos & Que bueno baila usted
#https://soundcloud.com/carlosg11/cali-pachanguero-grupo-niche // Cali Pachanguero (Grupo Niche)
#https://soundcloud.com/lviradio-intenet/salsa-tito-rodriguez-sun-sun // Salsa - tito rodriguez - sun sun babae
#https://soundcloud.com/thelavoz/el-gran-combo-de-puerto-rico-ojos-chinos // El Gran Combo de Puerto Rico - Ojos Chinos
#https://soundcloud.com/thelavoz/el-gran-combo-de-puerto-rico-y-no-hago-mas-na // El Gran Combo - de Puerto Rico - Y no hago mas na'
#https://soundcloud.com/salsavieja/el-gran-combo-de-puerto-rico // El Gran Combo De Puerto Rico Un Verano En New York
#https://soundcloud.com/monchymix/el-gran-combo-de-puerto-rico // El Gran Combo de Puerto Rico - Timbalero [1981]
#https://soundcloud.com/mario-xavier-orde-ana-ruiz/el-raton-homenaje-a-cheo-feliciano // El Raton (Cheo Feliciano)
#https://soundcloud.com/gilmar-sancan/cheo-feliciano-anacaona // Cheo feliciano - Anacaona
#https://soundcloud.com/huguin-1/hector-lavoe-live-el-cantante // Hector Lavoe Live- EL CANTANTE
#https://soundcloud.com/oliver-olivera-1/willie-colon-el-gran-varon // Willie Colon El Gran Varon
#https://soundcloud.com/salsa-1/43-gitana-willie-colon // Gitana - Willie Colon
#https://soundcloud.com/ricardo-sanabria-vega/idilio-willie-colon // Idilio (Willie Colon)
#https://soundcloud.com/republicofmusic/willie-colon-che-che-col // Willie Colon - Che Che Colé
#https://soundcloud.com/ricky-gonzalez-9/johnny-pacheco-azucar-mami // JOHNNY PACHECO - AZUCAR MAMI

def create_songs(s)
  s.each do |e|
    song = Song.new(name: e[:name], path: e[:path], genre: e[:genre])
    if (Artist.find_by(name: e[:artist]).nil?)
      artist = Artist.new(name: e[:artist])
      artist.save
      song.artist = artist
      song.save
    else
      song.artist = Artist.find_by(name: e[:artist])
      song.save
    end
  end
end

def create_users(u)
  u.each do |e|
    User.create(first_name: e[:first_name], last_name: e[:last_name], email: e[:email])
  end
end

s = [
  {name: "La hipocresia y la falsedad", path:"https://soundcloud.com/rickygonzalez-1/la-hipocresia-y-la-falsedad", genre: "Salsa", artist: "Ray Barreto"},
  {name: "Swing La Moderna", path:"https://soundcloud.com/djser-j/ray-barreto-swing-la-moderna", genre: "Salsa", artist: "Ray Barreto"},
  {name: "fuego y palante", path:"https://soundcloud.com/ritmoson/fuego-y-palante-ray-barreto", genre: "Salsa", artist: "Ray barreto"},
  {name: "Cienfuegos & Que bueno baila usted", path:"https://soundcloud.com/tumi-music/cienfuegos-que-bueno-baila", genre: "Salsa", artist: "benny more"},
  {name: "cali pachangero", path:"https://soundcloud.com/carlosg11/cali-pachanguero-grupo-niche", genre: "Salsa", artist: "grupo niche"},
  {name: "sun sun babae", path:"https://soundcloud.com/lviradio-intenet/salsa-tito-rodriguez-sun-sun", genre: "Salsa", artist: "tito rodriguez"},
  {name: "Azucar mami", path:"https://soundcloud.com/ricky-gonzalez-9/johnny-pacheco-azucar-mami", genre: "Salsa", artist: "johnny pacheco"},
  {name: "Che che cole", path:"https://soundcloud.com/republicofmusic/willie-colon-che-che-col", genre: "Salsa", artist: "willie colon"},
  {name: "idilio", path:"https://soundcloud.com/ricardo-sanabria-vega/idilio-willie-colon", genre: "Salsa", artist: "willie colon"},
  {name: "Gitana", path:"https://soundcloud.com/salsa-1/43-gitana-willie-colon", genre: "Salsa", artist: "willie colon"},
  {name: "El Gran varon", path:"https://soundcloud.com/oliver-olivera-1/willie-colon-el-gran-varon", genre: "SALSA", artist: "willie colon"},
  {name: "El cantante", path:"https://soundcloud.com/huguin-1/hector-lavoe-live-el-cantante", genre: "SALSA", artist: "hector lavoe"},
  {name: "Anacaona", path:"https://soundcloud.com/gilmar-sancan/cheo-feliciano-anacaona", genre: "SALSA", artist: "cheo feliciano"},
  {name: "el raton", path:"https://soundcloud.com/mario-xavier-orde-ana-ruiz/el-raton-homenaje-a-cheo-feliciano", genre: "SALSA", artist: "cheo feliciano"},
  {name: "timbalero", path:"https://soundcloud.com/monchymix/el-gran-combo-de-puerto-rico", genre: "SALSA", artist: "el gran combo de puerto rico"},
  {name: "un verano en nueva york", path:"https://soundcloud.com/salsavieja/el-gran-combo-de-puerto-rico", genre: "SALSA", artist: "el gran combo de puerto rico"},
  {name: "Y no hago mas na'", path:"https://soundcloud.com/thelavoz/el-gran-combo-de-puerto-rico-y-no-hago-mas-na", genre: "SALSA", artist: "el gran combo de puerto rico"},
  {name: "ojos chinos", path:"https://soundcloud.com/thelavoz/el-gran-combo-de-puerto-rico-ojos-chinos", genre: "SALSA", artist: "el gran combo de puerto rico"},
  {name: "Hay Fuego En El 23", path:"#https://soundcloud.com/jobo-antepaz-1/arsenio-rodriguez-su-conjunto", genre: "Salsa", artist: "Arsenio Rodriguez"},
  {name: "Ya Me Lo Dio", path:"https://soundcloud.com/timbamusic/ya-me-lo-dio-arsenio-rodriguez", genre: "Salsa", artist: "Arsenio Rodriguez"},
  {name: "Nanara Cai", path:"https://soundcloud.com/djluis-1/la-sonora-poncena", genre: "Salsa", artist: "La Sonora Ponceña"},
  {name: "Changuiri", path:"https://soundcloud.com/bescor/changuiri-sonora-ponce-a", genre: "Salsa", artist: "La Sonora Ponceña"},
  {name: "como mango", path:"https://soundcloud.com/capitalmundialdelasalsa/como-mango-sonora-ponce-a", genre: "Salsa", artist: "La Sonora Ponceña"},
  {name: "medley", path:"https://soundcloud.com/gonzalezsantana/sets/medley-sonora-poncena", genre: "Salsa", artist: "La Sonora Ponceña"},
  {name: "Ran Kan Kan", path:"https://soundcloud.com/afrocuba/05-ran-kan-kan-mp3", genre: "Salsa", artist: "Tito puente"}
]

u =[
  {first_name: "Johanna", last_name: "Salazar", email: "johanna.salazar.b@gmail.com"},
  {first_name: "Joselynne", last_name: "Salazar", email: "joselynne@gmail.com"},
  {first_name: "Maria", last_name: "Graterol", email: "Maria@gmail.com"},
  {first_name: "Cristina", last_name: "Lugo", email: "cris@gmail.com"},
  {first_name: "Daniel", last_name: "Ramirez", email: "daniel@gmail.com"}
] 

create_songs(s)
create_users(u)
f = Friend.new
f.requester = User.first
f.answerer = User.find(2)
f.save

c = Challenge.new
c.challenger = User.first
c.challenged = User.find(2)
c.genre = "Salsa"
c.save
