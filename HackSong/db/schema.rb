# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150815033407) do

  create_table "artist_genres", force: :cascade do |t|
    t.integer  "artist_id",  limit: 4
    t.string   "genre",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "artist_genres", ["artist_id"], name: "index_artist_genres_on_artist_id", using: :btree

  create_table "artists", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "artists_per_genres", force: :cascade do |t|
    t.string   "genre",      limit: 255
    t.integer  "quantity",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "challenges", force: :cascade do |t|
    t.integer  "challenger_id",                limit: 4
    t.integer  "challenged_id",                limit: 4
    t.string   "genre",                        limit: 255
    t.string   "status",                       limit: 255
    t.string   "winner",                       limit: 255
    t.integer  "correct_answers_challenger",   limit: 4
    t.integer  "correct_answers_challenged",   limit: 4
    t.integer  "song1_id",                     limit: 4
    t.string   "opt1_song1",                   limit: 255
    t.string   "opt2_song1",                   limit: 255
    t.string   "opt3_song1",                   limit: 255
    t.string   "opt4_song1",                   limit: 255
    t.string   "opt5_song1",                   limit: 255
    t.integer  "response_song1_challenger_id", limit: 4
    t.integer  "response_song1_challenged_id", limit: 4
    t.boolean  "song1_answered_challenger",                default: false
    t.boolean  "song1_answered_challenged",                default: false
    t.boolean  "song1_correctness_challenger"
    t.boolean  "song1_correctness_challenged"
    t.integer  "song2_id",                     limit: 4
    t.string   "opt1_song2",                   limit: 255
    t.string   "opt2_song2",                   limit: 255
    t.string   "opt3_song2",                   limit: 255
    t.string   "opt4_song2",                   limit: 255
    t.string   "opt5_song2",                   limit: 255
    t.integer  "response_song2_challenger_id", limit: 4
    t.integer  "response_song2_challenged_id", limit: 4
    t.boolean  "song2_answered_challenger",                default: false
    t.boolean  "song2_answered_challenged",                default: false
    t.boolean  "song2_correctness_challenger"
    t.boolean  "song2_correctness_challenged"
    t.integer  "song3_id",                     limit: 4
    t.string   "opt1_song3",                   limit: 255
    t.string   "opt2_song3",                   limit: 255
    t.string   "opt3_song3",                   limit: 255
    t.string   "opt4_song3",                   limit: 255
    t.string   "opt5_song3",                   limit: 255
    t.integer  "response_song3_challenger_id", limit: 4
    t.integer  "response_song3_challenged_id", limit: 4
    t.boolean  "song3_answered_challenger",                default: false
    t.boolean  "song3_answered_challenged",                default: false
    t.boolean  "song3_correctness_challenger"
    t.boolean  "song3_correctness_challenged"
    t.integer  "song4_id",                     limit: 4
    t.string   "opt1_song4",                   limit: 255
    t.string   "opt2_song4",                   limit: 255
    t.string   "opt3_song4",                   limit: 255
    t.string   "opt4_song4",                   limit: 255
    t.string   "opt5_song4",                   limit: 255
    t.integer  "response_song4_challenger_id", limit: 4
    t.integer  "response_song4_challenged_id", limit: 4
    t.boolean  "song4_answered_challenger",                default: false
    t.boolean  "song4_answered_challenged",                default: false
    t.boolean  "song4_correctness_challenger"
    t.boolean  "song4_correctness_challenged"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  add_index "challenges", ["challenged_id"], name: "index_challenges_on_challenged_id", using: :btree
  add_index "challenges", ["challenger_id"], name: "index_challenges_on_challenger_id", using: :btree
  add_index "challenges", ["response_song1_challenged_id"], name: "index_challenges_on_response_song1_challenged_id", using: :btree
  add_index "challenges", ["response_song1_challenger_id"], name: "index_challenges_on_response_song1_challenger_id", using: :btree
  add_index "challenges", ["response_song2_challenged_id"], name: "index_challenges_on_response_song2_challenged_id", using: :btree
  add_index "challenges", ["response_song2_challenger_id"], name: "index_challenges_on_response_song2_challenger_id", using: :btree
  add_index "challenges", ["response_song3_challenged_id"], name: "index_challenges_on_response_song3_challenged_id", using: :btree
  add_index "challenges", ["response_song3_challenger_id"], name: "index_challenges_on_response_song3_challenger_id", using: :btree
  add_index "challenges", ["response_song4_challenged_id"], name: "index_challenges_on_response_song4_challenged_id", using: :btree
  add_index "challenges", ["response_song4_challenger_id"], name: "index_challenges_on_response_song4_challenger_id", using: :btree
  add_index "challenges", ["song1_id"], name: "index_challenges_on_song1_id", using: :btree
  add_index "challenges", ["song2_id"], name: "index_challenges_on_song2_id", using: :btree
  add_index "challenges", ["song3_id"], name: "index_challenges_on_song3_id", using: :btree
  add_index "challenges", ["song4_id"], name: "index_challenges_on_song4_id", using: :btree

  create_table "friends", force: :cascade do |t|
    t.integer  "requester_id", limit: 4
    t.integer  "answerer_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "friends", ["answerer_id"], name: "index_friends_on_answerer_id", using: :btree
  add_index "friends", ["requester_id"], name: "index_friends_on_requester_id", using: :btree

  create_table "songs", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "path",        limit: 150
    t.string   "genre",       limit: 255
    t.integer  "artist_id",   limit: 4
    t.integer  "genre_index", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "songs", ["artist_id"], name: "index_songs_on_artist_id", using: :btree
  add_index "songs", ["genre_index"], name: "index_songs_on_genre_index", using: :btree

  create_table "songs_per_genres", force: :cascade do |t|
    t.string   "genre",      limit: 255
    t.integer  "quantity",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",     limit: 255
    t.string   "last_name",      limit: 255
    t.string   "email",          limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "winning_number", limit: 4
    t.integer  "loss_number",    limit: 4
  end

  add_foreign_key "artist_genres", "artists"
end
