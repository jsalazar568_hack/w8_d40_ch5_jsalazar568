class AddLossNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :loss_number, :integer
  end
end
