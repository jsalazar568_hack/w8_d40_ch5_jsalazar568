class AddWinningNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :winning_number, :integer
  end
end
