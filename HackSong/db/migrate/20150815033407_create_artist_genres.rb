class CreateArtistGenres < ActiveRecord::Migration
  def change
    create_table :artist_genres do |t|
      t.references :artist, index: true, foreign_key: true
      t.string :genre

      t.timestamps null: false
    end
  end
end
