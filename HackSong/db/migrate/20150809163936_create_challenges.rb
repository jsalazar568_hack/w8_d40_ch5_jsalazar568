class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.integer :challenger_id, index: true
      t.integer :challenged_id, index: true
      t.string :genre
      t.string :status
      t.string :winner
      t.integer :correct_answers_challenger
      t.integer :correct_answers_challenged
      t.integer :song1_id, index: true
      t.string :opt1_song1
      t.string :opt2_song1
      t.string :opt3_song1
      t.string :opt4_song1
      t.string :opt5_song1
      t.integer :response_song1_challenger_id, index: true
      t.integer :response_song1_challenged_id, index: true
      t.boolean :song1_answered_challenger, default: false
      t.boolean :song1_answered_challenged, default: false
      t.boolean :song1_correctness_challenger
      t.boolean :song1_correctness_challenged
      t.integer :song2_id, index: true
      t.string :opt1_song2
      t.string :opt2_song2
      t.string :opt3_song2
      t.string :opt4_song2
      t.string :opt5_song2
      t.integer :response_song2_challenger_id, index: true
      t.integer :response_song2_challenged_id, index: true
      t.boolean :song2_answered_challenger, default: false
      t.boolean :song2_answered_challenged, default: false
      t.boolean :song2_correctness_challenger
      t.boolean :song2_correctness_challenged
      t.integer :song3_id, index: true
      t.string :opt1_song3
      t.string :opt2_song3
      t.string :opt3_song3
      t.string :opt4_song3
      t.string :opt5_song3
      t.integer :response_song3_challenger_id, index: true
      t.integer :response_song3_challenged_id, index: true
      t.boolean :song3_answered_challenger, default: false
      t.boolean :song3_answered_challenged, default: false
      t.boolean :song3_correctness_challenger
      t.boolean :song3_correctness_challenged
      t.integer :song4_id, index: true
      t.string :opt1_song4
      t.string :opt2_song4
      t.string :opt3_song4
      t.string :opt4_song4
      t.string :opt5_song4
      t.integer :response_song4_challenger_id, index: true
      t.integer :response_song4_challenged_id, index: true
      t.boolean :song4_answered_challenger, default: false
      t.boolean :song4_answered_challenged, default: false
      t.boolean :song4_correctness_challenger
      t.boolean :song4_correctness_challenged

      t.timestamps null: false
    end
  end
end