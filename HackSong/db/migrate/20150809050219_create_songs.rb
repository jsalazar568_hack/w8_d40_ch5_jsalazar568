class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :name
      t.string :path, limit: 150
      t.string :genre
      t.integer :artist_id, index: true
      t.integer :genre_index, index:true

      t.timestamps null: false
    end
  end
end
