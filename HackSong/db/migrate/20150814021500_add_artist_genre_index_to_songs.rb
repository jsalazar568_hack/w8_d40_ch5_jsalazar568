class AddArtistGenreIndexToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :artist_genre_index, :integer, index: true
  end
end
