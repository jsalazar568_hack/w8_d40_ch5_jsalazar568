class CreateSongsPerGenres < ActiveRecord::Migration
  def change
    create_table :songs_per_genres do |t|
      t.string :genre
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
