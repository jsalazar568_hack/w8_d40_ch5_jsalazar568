class CreateArtistsPerGenres < ActiveRecord::Migration
  def change
    create_table :artists_per_genres do |t|
      #t.references :artist, index: true, foreign_key: true
      t.string :genre
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
