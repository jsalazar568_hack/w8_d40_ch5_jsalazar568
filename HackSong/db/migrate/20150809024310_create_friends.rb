class CreateFriends < ActiveRecord::Migration
  #http://stackoverflow.com/questions/14867981/how-do-i-add-migration-with-multiple-references-to-the-same-model-in-one-table
  def change
    create_table :friends do |t|
      t.integer :requester_id, index: true
      t.integer :answerer_id, index: true

      t.timestamps null: false
    end
  end
end
