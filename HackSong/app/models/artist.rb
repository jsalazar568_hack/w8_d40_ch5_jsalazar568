class Artist < ActiveRecord::Base
  has_many :songs, :class_name => 'Song', :foreign_key => 'artist_id'
  has_many :artist_genres

  validates :name, presence: true, uniqueness: true

  before_validation :normalize, on: :create

  def normalize
    self.name = self.name.downcase.titleize
  end 
end
