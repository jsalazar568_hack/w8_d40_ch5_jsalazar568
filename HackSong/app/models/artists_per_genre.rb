class ArtistsPerGenre < ActiveRecord::Base
  # belongs_to :artist

  validates :genre, uniqueness: true
  validates :quantity, numericality: {only_integer: true}
end
