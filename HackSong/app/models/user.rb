class User < ActiveRecord::Base
  # http://www.spacevatican.org/2008/5/6/creating-multiple-associations-with-the-same-table/
  has_many :requests, :class_name => 'Friend', :foreign_key => 'requester_id'
  has_many :answers, :class_name => 'Friend', :foreign_key => 'answerer_id'

  has_many :requested_challenges, :class_name => 'Challenge', :foreign_key => 'challenger_id'
  has_many :given_challenges, :class_name => 'Challenge', :foreign_key => 'challenged_id'
  #has_many :winnings, :class_name => 'Challenge', :foreign_key => 'winner_id'

  validates :first_name, :last_name, :email, presence: true
  validates :email, uniqueness: true
  validates :email, format: {with: /[\w.-]+@([\w-]\.?)+/, message: "InvalidEmailFormat"}

  #OJO con la columna picture

  before_save :normalize, :set_start, on: :create

  def normalize
    self.first_name = self.first_name.downcase.titleize
    self.last_name = self.last_name.downcase.titleize
    self.email = self.email.downcase
  end

  def full_name
    [self.first_name, self.last_name].join(" ")
  end

  private

  def set_start
    self.winning_number = 0
    self.loss_number = 0
  end

end