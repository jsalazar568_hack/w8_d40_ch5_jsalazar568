class Friend < ActiveRecord::Base
  # http://www.spacevatican.org/2008/5/6/creating-multiple-associations-with-the-same-table/
  # http://stackoverflow.com/questions/14867981/how-do-i-add-migration-with-multiple-references-to-the-same-model-in-one-table
  belongs_to :requester, :class_name => 'User', :foreign_key => 'requester_id'
  belongs_to :answerer, :class_name => 'User', :foreign_key => 'answerer_id'

  # f.answerer = User.find(1)
  # f.requester = User.find(2)
  validates :requester, :answerer, presence: true
  validates :requester, uniqueness: {scope: :answerer}
  validate :same_player
  after_save :create_double_relation , on: :create

  def create_double_relation
    # http://stackoverflow.com/questions/16717077/how-to-validate-unique-pairs-in-rails
    friendship = Friend.new
    friendship.answerer = self.requester
    friendship.requester = self.answerer
    friendship.save
  end

  def same_player
    if (self.requester.nil? || self.answerer.nil?)
      return
    else
      if (self.requester == self.answerer)
        errors.add(:requester, "FriendshipError")
      end
    end
  end

end
