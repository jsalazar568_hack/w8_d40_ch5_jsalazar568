class ArtistGenre < ActiveRecord::Base
  belongs_to :artist

  validates :genre, presence: true

  # has_many :songs, :class_name => 'Song', :foreign_key => 'artist_id'
  # belongs_to :artist, :class_name => 'Artist', :foreign_key => 'artist_id'
end
