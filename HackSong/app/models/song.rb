class Song < ActiveRecord::Base
  #NO!belongs_to :genre_index, :class_name => 'SongsPerGenre', :foreign_key => 'genre_index'
  belongs_to :artist, :class_name => 'Artist', :foreign_key => 'artist_id'

  has_many :song1, :class_name => 'Challenge', :foreign_key => 'song1_id'
  has_many :response_song1_challenger, :class_name => 'Challenge', :foreign_key => 'response_song1_challenger_id'
  has_many :response_song1_challenged, :class_name => 'Challenge', :foreign_key => 'response_song1_challenged_id'
  has_many :song2, :class_name => 'Challenge', :foreign_key => 'song2_id'
  has_many :response_song2_challenger, :class_name => 'Challenge', :foreign_key => 'response_song2_challenger_id'
  has_many :response_song2_challenged, :class_name => 'Challenge', :foreign_key => 'response_song2_challenged_id'
  has_many :song3, :class_name => 'Challenge', :foreign_key => 'song3_id'
  has_many :response_song3_challenger, :class_name => 'Challenge', :foreign_key => 'response_song3_challenger_id'
  has_many :response_song3_challenged, :class_name => 'Challenge', :foreign_key => 'response_song3_challenged_id'
  has_many :song4, :class_name => 'Challenge', :foreign_key => 'song4_id'
  # has_many :opt1_song4, :class_name => 'Challenge', :foreign_key => 'opt1_song4_id'
  has_many :response_song4_challenger, :class_name => 'Challenge', :foreign_key => 'response_song4_challenger_id'
  has_many :response_song4_challenged, :class_name => 'Challenge', :foreign_key => 'response_song4_challenged_id'

  validates :name, :path, :genre, presence: true
  validates :path, uniqueness: true

  before_save :genre_quantify, :artist_quantify
  before_save :normalize, on: :create

  def normalize
    self.name = self.name.downcase.titleize
    self.genre = self.genre.downcase.titleize
  end

  private

  def genre_quantify
    self.normalize
    genreq = SongsPerGenre.find_by(genre: self.genre)
    if (!genreq.nil?)
      genreq.quantity += 1
      self.genre_index = genreq.quantity
      genreq.save
    else
      SongsPerGenre.create(genre: self.genre, quantity: 1)
      self.genre_index = 1
    end
  end

  def artist_quantify
    self.normalize
    artistq = ArtistsPerGenre.find_by(genre: self.genre)
    if (!artistq.nil?)
      artist = Artist.find_by(name: self.artist)
      #unless (artist) NO ES NECESARIO, porque la instancia de la cancion requiere la 
      #   instancia del artista (se genera antes que la cancion)
      if (!ArtistGenre.where(["artist_id = ? and genre = ?", artist.id, self.genre]).take)
        ag = ArtistGenre.new
        ag.artist = self.artist
        ag.genre = self.genre
        ag.save
        artistq.quantity += 1
        #self.artist_genre_index = artistq.quantity
        artistq.save
      end
      # Si existe la combinacion Artista-Genero no es necesario hacer nada
      #end
    else
      #http://api.rubyonrails.org/classes/ActiveRecord/Persistence/ClassMethods.html
      # ArtistsPerGenre.create(genre: self.genre, quantity: 1) do |a|
      #   a.artist = self.artist
      # end
      ArtistsPerGenre.create(genre: self.genre, quantity: 1)
      #self.artist_genre_index = 1 # artist_genre_index es igual al id correspondiente en ArtistGenre
      ag = ArtistGenre.new
      ag.artist = self.artist
      ag.genre = self.genre
      ag.save
    end
  end

end
