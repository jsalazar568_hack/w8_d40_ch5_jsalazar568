class SongsPerGenre < ActiveRecord::Base
  #has_many :songs, :class_name => 'Song', :foreign_key => "genre_index"

  validates :genre, uniqueness: true
  validates :quantity, numericality: {only_integer: true}

end
