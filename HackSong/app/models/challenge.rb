class Challenge < ActiveRecord::Base
  belongs_to :challenger, :class_name => 'User', :foreign_key => 'challenger_id'
  belongs_to :challenged, :class_name => 'User', :foreign_key => 'challenged_id'
  #belongs_to :winner, :class_name => 'User', :foreign_key => 'winner_id'

  belongs_to :song1, :class_name => 'Song', :foreign_key => 'song1_id'
  belongs_to :song2, :class_name => 'Song', :foreign_key => 'song2_id'
  belongs_to :song3, :class_name => 'Song', :foreign_key => 'song3_id'
  belongs_to :song4, :class_name => 'Song', :foreign_key => 'song4_id'
  # belongs_to :opt1_song4, :class_name => 'Song', :foreign_key => 'opt1_song4_id'

  #NO ES NECESARIO verificar que no se haga la misma relacion ej: 1-2 y 2-1
  validates :challenger_id, :challenged_id, :genre, presence: true
  validates :response_song1_challenger_id, :response_song2_challenger_id, :response_song3_challenger_id, :response_song4_challenger_id,
            :response_song1_challenged_id, :response_song2_challenged_id, :response_song3_challenged_id, :response_song4_challenged_id, 
            numericality: {only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5}, on: :update, allow_nil: true
  validate :friendship_existence, :genre_existence
  #before_save :deadline

  before_create :random_challenge_generation, on: :create
  before_save :normalize
  before_save :answered_status_assignment, :count_correct_answers, :check_winner, on: [:update]

  def normalize
    self.genre = self.genre.downcase.titleize
  end

  private

  # con un call_back, revisar cuanto tiempo tiene el reto y borrarlo si tiene mas de 3 dias
  def random_challenge_generation
    self.status = "playing"
    selected_song_array = []
    while (selected_song_array.length < 4)
      e = 1 + rand(SongsPerGenre.find_by(genre: self.genre).quantity)
      if (!selected_song_array.include?(e))
        selected_song_array.push(e)
      end
    end

    self.song1 = Song.where(["genre = ? and genre_index = ?", self.genre, selected_song_array[0]]).take
    self.song2 = Song.where(["genre = ? and genre_index = ?", self.genre, selected_song_array[1]]).take
    self.song3 = Song.where(["genre = ? and genre_index = ?", self.genre, selected_song_array[2]]).take
    self.song4 = Song.where(["genre = ? and genre_index = ?", self.genre, selected_song_array[3]]).take

    for i in 1..4
      options_song_array = []
      j = 1
      correct_answer_position = 1 + rand(5) 
      while (j <= 5)
        if (j == correct_answer_position)
          #http://stackoverflow.com/questions/4637321/self-sendmethod-value-does-not-work
          temp  = self.send("song#{i}").artist.name
          self.send("opt#{j}_song#{i}=", temp) 
          options_song_array.push(temp)
          j += 1
        else
          e = 1 + rand(ArtistsPerGenre.find_by(genre: self.genre).quantity)
          artist_selected = ArtistGenre.where(["genre = ? and id = ?", self.genre, e]).take.artist.name
          #SE SUPONE QUE CON artist_genre_index NO ES NECESARIO VERIFICAR !options_song_array.include?(artist_selected)
          # con artist_genre_index vas al artista de ese genero y ya
          # # artist_genre_index es igual al id correspondiente en ArtistGenre
          if (!options_song_array.include?(artist_selected) && artist_selected != self.send("song#{i}").artist.name)
            self.send("opt#{j}_song#{i}=", artist_selected)
            options_song_array.push(artist_selected)
            j += 1
          end
        end 
      end
    end

    # g = []
    # c = Challenge.first
    # Es posible hacer: 
    # g.push(c.challenger) !!!!!!!!!!!
    # UN ARRAY PUEDE GUARDAR REFERENCIAS! Era posible usar esto en este caso que la BD es Relacional? (No
    #   permite guardar matrices como una variable)
    # COMO HUBIERA PODIDO EVITAR TODO EL CODIGO DE ABAJO?

    # self.opt1_song1 = Song.where(["genre = ? and genre_index = ?", self.genre, options_array[0][0]]).take
    # self.opt2_song1 = Song.where(["genre = ? and genre_index = ?", self.genre, options_array[0][1]]).take ...

  end

  #DISCLAIMER
  #La elaboracion del siguiente codigo y todo codigo que sigue la misma logica de asignacion de variables 
  #en el presente documento fue requerida posteriormene a la firma de la clausula firmada en clase.
  #  Firma:  Johanna M. Salazar Bove

  def answered_status_assignment
    for i in 1..4
      unless (self.send("response_song#{i}_challenger_id").nil?)
        #http://stackoverflow.com/questions/4637321/self-sendmethod-value-does-not-work
        self.send("song#{i}_answered_challenger=", true)
      end
    end

    for i in 1..4
      unless (self.send("response_song#{i}_challenged_id").nil?)
        self.send("song#{i}_answered_challenged=", true)
      end
    end
  end

  #before_save: verifica SI SE CONTESTARON LAS 4 CANCIONES, SI ES ASI comparar con el artista real en la BD de canciones
  def count_correct_answers
    unless (self.response_song1_challenger_id.nil? || self.response_song2_challenger_id.nil? || self.response_song3_challenger_id.nil? || self.response_song4_challenger_id.nil?)
      self.correct_answers_challenger = 0
      # if (self.response_song1_challenger_id == 1) 
      #   self.song1.artist == self.opt1_song1 ? self.correct_answers_challenger += 1 : self.correct_answers_challenger = self.correct_answers_challenger
      # elsif (self.response_song1_challenger_id == 2) 
      #   self.song1.artist == self.opt2_song1 ? self.correct_answers_challenger += 1 : self.correct_answers_challenger = self.correct_answers_challenger
      # elsif (self.response_song1_challenger_id == 3) 
      #   self.song1.artist == self.opt3_song1 ? self.correct_answers_challenger += 1 : self.correct_answers_challenger = self.correct_answers_challenger
      # elsif (self.response_song1_challenger_id == 4) 
      #   self.song1.artist == self.opt4_song1 ? self.correct_answers_challenger += 1 : self.correct_answers_challenger = self.correct_answers_challenger
      # elsif (self.response_song1_challenger_id == 5) 
      #   self.song1.artist == self.opt5_song1 ? self.correct_answers_challenger += 1 : self.correct_answers_challenger = self.correct_answers_challenger
      # end
      # Otro para song2, song3 y song4. Luego lo mismo para el jugador "challenged"

      for j in 1..4
        for i in 1..5
          if (self.send("response_song#{j}_challenger_id") == i)
            #self.send("opt#{i}_song1").artist == self.send("song#{j}").artist ? self.send("song#{j}_correctness_challenger") = true : self.send("song#{j}_correctness_challenger") = false
            if (self.send("opt#{i}_song#{j}") == self.send("song#{j}").artist.name)
              self.send("song#{j}_correctness_challenger=", true)
              self.correct_answers_challenger += 1
            else
              self.send("song#{j}_correctness_challenger=", false)
            end
          end
        end
      end
    end

    unless (self.response_song1_challenged_id.nil? || self.response_song2_challenged_id.nil? || self.response_song3_challenged_id.nil? || self.response_song4_challenged_id.nil?)
      self.correct_answers_challenged = 0
      for j in 1..4
        for i in 1..5
          if (self.send("response_song#{j}_challenged_id") == i)
            if (self.send("opt#{i}_song#{j}") == self.send("song#{j}").artist.name)
              self.send("song#{j}_correctness_challenged=", true)
              self.correct_answers_challenged += 1
            else
              self.send("song#{j}_correctness_challenged=", false)
            end
          end
        end
      end
    end
  end

  def check_winner
    unless (self.correct_answers_challenged.nil? || self.correct_answers_challenger.nil?)
      self.status = "ended"
      if (self.correct_answers_challenger > self.correct_answers_challenged)
        self.winner = self.challenger.full_name
        self.challenger.winning_number += 1
      elsif (self.correct_answers_challenger < self.correct_answers_challenged)
        self.winner = self.challenged.full_name
        self.challenged.winning_number += 1
      elsif (self.correct_answers_challenger == self.correct_answers_challenged)
        self.winner = "tie"
      end
    end
  end

  def friendship_existence
    if (self.challenger.nil? || self.challenged.nil?)
      return
    else
      if (self.challenger.requests.find_by(answerer: self.challenged.id).nil?)
        errors.add(:challenger, "FriendshipMissing")
      end
    end
  end

  def genre_existence
    if (self.genre.nil?)
      return
    else
      if (SongsPerGenre.find_by(genre: self.genre).nil?)        
        errors.add(:genre, "GenreNotFound")
      end
    end
  end

  # def validate_deadline
  #   self.status = "expired"
  #   if self.deadline && self.deadline < Time.now
  #     errors.add(:deadline, "La fecha tiene que ser mayor al dia actual")
  #   end
  # end

end
