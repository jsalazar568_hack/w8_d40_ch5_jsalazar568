class SongsController < ApplicationController
  def create
    # song = Song.new(permit_params.select{ |key, value| key.to_s.match(/^(name|path|genre)$/)})
    song = Song.new(permit_params)
    artist = Artist.new(name: params[:artist])
    if (artist.save)
      puts ("artista salvo!")
      song.artist = artist
    else
      puts ("artista no salvo!")
      song.artist = Artist.find_by(name: params[:artist])
    end
    if (song.save)
      render json:create_view(song)
    else
      render json:{errors: song.errors.full_message, message: "ValidationError"}, status: :bad_request
    end
  end

  private

  def permit_params 
    if (params[:song].present?)
      #params.require(:song).permit(:name, :path, :genre, :artist)
      params.require(:song).permit(:name, :path, :genre)
    else
      render json:{errors: song.errors.full_message, message: "InvalidParameters"}, status: :bad_request
    end
  end

  def create_view(song)
    {
      name: song.name,
      path: song.path,
      genre: song.genre,
      artist: song.artist.name
    }
  end
end

#https://soundcloud.com/newdimmension2/tame-impala-gossip-borby-norton-remix
#https://soundcloud.com/tracks/539174
#https://soundcloud.com/matisyahu/live-in-vail-feb-13-2015
#https://developers.soundcloud.com/docs/api/reference#tracks