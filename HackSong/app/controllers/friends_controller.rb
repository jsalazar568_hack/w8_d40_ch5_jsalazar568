class FriendsController < ApplicationController
  
  before_filter :identify_requester
  before_filter :identify_answerer, only: :create

  def index
    # No lo necesito por haberlo salvado doble (como requester y answerer ej: 1-2 y 2-1)
    # list = @requester.requests
    # list.push(@requester.answers)
    render json: create_full_view(@requester.requests)
  end

  def create
    friendship = Friend.new
    friendship.requester = @requester
    friendship.answerer = @answerer
    if (friendship.save)
      render json: create_view
      # rescue ActiveRecord::RecordNotUnique => e
      #   render json: {message: "RecordNotUnique"}, status: :not_acceptable
    else
      render json:{errors: friendship.errors.full_message, message: "ValidationError"}, status: :bad_request
    end
  end

  private

  def permit_params
    if (params[:friend].present?)
      params.require(:friend).permit(:id)
    end
  end

  def identify_answerer
    @answerer = User.find(permit_params[:id])
  end

  def identify_requester
    @requester = User.find(params[:user_id])
  end

  def create_view
    {
      requester: @requester.full_name,
      answerer: @answerer.full_name
    }
  end

  def create_full_view(friendships_array)
    view = []
    friendships_array.each do |friendship|
      view.push({
        requester: friendship.requester.full_name,
        winning_number_requester: friendship.requester.winning_number,
        answerer: friendship.answerer.full_name,
        winning_number_answerer: friendship.answerer.winning_number
        })
    end
    return view
  end
end
