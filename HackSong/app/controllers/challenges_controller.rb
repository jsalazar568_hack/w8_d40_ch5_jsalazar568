class ChallengesController < ApplicationController
  before_filter :identify_challenger
  before_filter :identify_challenged, only: :create

  def create
    # POST   /users/:user_id/challenges(.:format)          challenges#create
    challenge = Challenge.new
    challenge.genre = permit_params[:genre]
    challenge.challenger = @challenger
    challenge.challenged = @challenged
    #!!!!!!!!!!!!!!!NO se puede mostrar toda la info de challenge!
    if (challenge.save)
      render json: create_view(challenge)
    else 
      render json:{errors: challenge.errors.full_message, message: "ValidationError"}, status: :bad_request
    end
  end

  def index
    # user_challenges GET    /users/:user_id/challenges(.:format)          challenges#index
    begin
      view = create_simplified_view(Challenge.where(["challenger_id = ? or challenged_id = ?", params[:user_id], params[:user_id]]))
      render json: view
    rescue ActiveRecord::RecordNotFound => e
      render json: {message: "ChallengeNotFound"}, status: :not_found
    end
  end

  def show
    # user_challenge GET    /users/:user_id/challenges/:id(.:format)      challenges#show
    begin
      render json: create_view(Challenge.find(params[:id]))
    rescue ActiveRecord::RecordNotFound => e
      render json: {message: "ChallengeNotFound"}, status: :not_found
    end
  end

  def update
    # PUT    /users/:user_id/challenges/:id(.:format)      challenges#update
    params.permit(:response_song1, :response_song2, :response_song3, :response_song4, :user_id, :id)
    begin
      challenge = Challenge.find(params[:id])
      # if (!params[:challenge].present?)
      #   render json:{errors: challenge.errors.full_message, message: "InvalidParameters"}, status: :bad_request
      # else
      #  params.require(:challenge).permit(:response_song1, :response_song2, :response_song3, :response_song4)
        challenge = assign_response(challenge)
        if challenge.save
          render json: create_view(challenge)
        else
          render json: {message: "ValidationError"}, status: :bad_request
        end
      # end
    rescue ActiveRecord::RecordNotFound => e
      render json: {message: "ChallengeNotFound"}, status: :not_found
    end
  end

  private

  def assign_response(challenge)
    if (challenge.challenger_id == params[:user_id].to_i)
      challenge.response_song1_challenger_id = params[:response_song1]
      challenge.response_song2_challenger_id = params[:response_song2]
      challenge.response_song3_challenger_id = params[:response_song3]
      challenge.response_song4_challenger_id = params[:response_song4]
    elsif (challenge.challenged_id == params[:user_id].to_i)
      challenge.response_song1_challenged_id = params[:response_song1]
      challenge.response_song2_challenged_id = params[:response_song2]
      challenge.response_song3_challenged_id = params[:response_song3]
      challenge.response_song4_challenged_id = params[:response_song4]
    else
      render json: {message: "NotAGamer"}, status: :bad_request
    end
    return challenge
  end

  def permit_params
    if (params[:challenge].present?)
      params.require(:challenge).permit(:genre)
    else
      render json:{errors: challenge.errors.full_message, message: "InvalidParameters"}, status: :bad_request
    end
  end

  # def permit_params_update
  #   if (params[:challenge].present?)
  #     params.require(:challenge).permit(:response_song1, :response_song2, :response_song3, :response_song4)
  #   else
  #     render json:{errors: challenge.errors.full_message, message: "InvalidParameters"}, status: :bad_request
  #   end
  # end

  def identify_challenged
    @challenged = User.find(params[:user])
  end

  def identify_challenger
    @challenger = User.find(params[:user_id])
  end

  def create_simplified_view(challenges_array)
    # No mostrar los retos vencidos
    view = []
    challenges_array.each do |challenge|
      view.push({
        id: challenge.id,
        song: 
            { 
              name: challenge.song1.name,
              path: challenge.song1.path,
              genre: challenge.song1.genre
            },
        challenger: challenge.challenger.full_name,
        record_winnings_challenger: challenge.challenger.winning_number,
        record_losses_challenger: challenge.challenger.loss_number,
        challenged: challenge.challenged.full_name,
        record_winnings_challenged: challenge.challenged.winning_number,
        record_losses_challenged: challenge.challenged.loss_number,
        correct_answers_challenger: challenge.correct_answers_challenger,
        correct_answers_challenged: challenge.correct_answers_challenged,
        winner: challenge.winner,
        status: challenge.status,
        })
    end
    return view
  end

  def create_view(challenge)
    {
      id: challenge.id,
      challenger: challenge.challenger.full_name,
      challenged: challenge.challenged.full_name, 
      status: challenge.status,
      winner: challenge.winner,
      correct_answers_challenger: challenge.correct_answers_challenger,
      correct_answers_challenged: challenge.correct_answers_challenged,
      questions: [
        {
          song: 
            { 
              name: challenge.song1.name,
              path: challenge.song1.path,
              genre: challenge.song1.genre
            },
          options:
            { 
              opt1_song: challenge.opt1_song1,
              opt2_song: challenge.opt2_song1,
              opt3_song: challenge.opt3_song1,
              opt4_song: challenge.opt4_song1,
              opt5_song: challenge.opt5_song1
            },
          answered: 
            {
              challenger: challenge.song1_answered_challenger,
              challenged: challenge.song1_answered_challenged
            },
          answers: 
            {
              challenger: challenge.response_song1_challenger_id,
              challenged: challenge.response_song1_challenged_id
            }
        },
        {
          song: 
            { 
              name: challenge.song2.name,
              path: challenge.song2.path,
              genre: challenge.song2.genre
            },
          options:
            { 
              opt1_song: challenge.opt1_song2,
              opt2_song: challenge.opt2_song2,
              opt3_song: challenge.opt3_song2,
              opt4_song: challenge.opt4_song2,
              opt5_song: challenge.opt5_song2
            },
          answered: 
            {
              challenger: challenge.song2_answered_challenger,
              challenged: challenge.song2_answered_challenged
            },
          answers: 
            {
              challenger: challenge.response_song2_challenger_id,
              challenged: challenge.response_song2_challenged_id
            }
        },
         {
          song: 
            { 
              name: challenge.song3.name,
              path: challenge.song3.path,
              genre: challenge.song3.genre
            },
          options:
            { 
              opt1_song: challenge.opt1_song3,
              opt2_song: challenge.opt2_song3,
              opt3_song: challenge.opt3_song3,
              opt4_song: challenge.opt4_song3,
              opt5_song: challenge.opt5_song3
            },
          answered: 
            {
              challenger: challenge.song3_answered_challenger,
              challenged: challenge.song3_answered_challenged
            },
          answers: 
            {
              challenger: challenge.response_song3_challenger_id,
              challenged: challenge.response_song3_challenged_id
            }
        },
         {
          song: 
            { 
              name: challenge.song4.name,
              path: challenge.song4.path,
              genre: challenge.song4.genre
            },
          options:
            { 
              opt1_song: challenge.opt1_song4,
              opt2_song: challenge.opt2_song4,
              opt3_song: challenge.opt3_song4,
              opt4_song: challenge.opt4_song4,
              opt5_song: challenge.opt5_song4
            },
          answered: 
            {
              challenger: challenge.song4_answered_challenger,
              challenged: challenge.song4_answered_challenged
            },
          answers: 
            {
              challenger: challenge.response_song4_challenger_id,
              challenged: challenge.response_song4_challenged_id
            }
        }
      ], 
    }
  end

end