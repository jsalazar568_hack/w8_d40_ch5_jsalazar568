class UsersController < ApplicationController

  def index
    users = nil
    users = User.all

    render json: create_full_view(users)

  end

  def create
    user = User.new(permit_params)
    if (user.save)
      render json: create_view(user)
    else
      render json:{errors: user.errors.full_message, message: "ValidationError"}, status: :bad_request
    end

    # Probar
    # if (params.permitted?)
    # Como usar el permitted? Toma en cuenta lq restriccion que hice abajo?
    #   user = User.create(permit_params)
    #   render json: user
    # else
    #   render json:{errors: user.errors.full_message, message: "ValidationError"}, status: :bad_request
    # end  
  end

  private 

  def permit_params
    # OJOOOOOOOOO con picture
    if (params[:user].present?)
      params.require(:user).permit(:first_name, :last_name, :email)
    end
  end
  # http://api.rubyonrails.org/classes/ActionController/Parameters.html
  # Probar:
  # Necesito en verdad el if, si estoy haciendo require?
  # permitted = params.require(:user).permit(:first_name, :last_name, :email)

  def create_view(user)
    {
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      winning_number: user.winning_number,
      loss_number: user.loss_number
    }
  end

  def create_full_view(users_array)
    view = []
    users_array.each do |user|
      view.push(create_view(user))
    end
    return view
  end
end